FROM condaforge/miniforge3:latest

RUN apt-get update -y
RUN apt-get install -y pandoc libgl1-mesa-glx build-essential jq

ENV HTTP_PROXY="http://proxy.esrf.fr:3128/"
ENV HTTPS_PROXY="http://proxy.esrf.fr:3128/"
ENV NO_PROXY="esrf.fr,localhost"
