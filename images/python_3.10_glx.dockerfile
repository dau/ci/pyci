FROM python:3.10
ENV HTTP_PROXY="http://proxy.esrf.fr:3128/"
ENV HTTPS_PROXY="http://proxy.esrf.fr:3128/"
ENV NO_PROXY="esrf.fr,localhost"

RUN apt-get update -y
RUN apt-get install -y build-essential jq
RUN apt-get install -y libgl1-mesa-glx
RUN apt-get install -y xvfb
RUN apt-get install -y python3-pyqt5
RUN apt-get install -y python3-pyqt6
