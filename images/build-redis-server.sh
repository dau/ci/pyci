#!/bin/bash

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function main () {
    export version=$1

    cd ${SCRIPT_ROOT}
    rm -rf redis-${version}

    curl -O http://download.redis.io/releases/redis-${version}.tar.gz
    tar xzvf redis-${version}.tar.gz
    cd redis-${version}

    make

    make install

    mkdir -p /etc/redis
    rm -rf /etc/redis/redis.conf
    touch /etc/redis/redis.conf
}

main "$@"
