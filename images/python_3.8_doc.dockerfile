FROM python:3.8
ENV HTTP_PROXY="http://proxy.esrf.fr:3128/"
ENV HTTPS_PROXY="http://proxy.esrf.fr:3128/"
ENV NO_PROXY="esrf.fr,localhost"

RUN apt-get update -y
RUN apt-get install -y build-essential jq
RUN apt-get install -y pandoc
RUN apt-get install -y libgl1-mesa-glx
