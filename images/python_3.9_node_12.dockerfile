FROM python:3.9
ENV HTTP_PROXY="http://proxy.esrf.fr:3128/"
ENV HTTPS_PROXY="http://proxy.esrf.fr:3128/"
ENV NO_PROXY="esrf.fr,localhost"

RUN apt-get update -y
RUN apt-get install -y build-essential jq

RUN apt-get install -y npm
RUN npm -v

RUN npm install -g npx
RUN npx -v

RUN npm install -g n
RUN n 12.22.7
RUN node -v
