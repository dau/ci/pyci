#!/bin/bash

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function main () {
    export version=$1

    cd ${SCRIPT_ROOT}
    rm -rf RedisJSON

    git -c advice.detachedHead=false clone https://github.com/RedisJSON/RedisJSON.git --branch v${version}
    cd RedisJSON

    cargo build --release

    mkdir -p /usr/local/lib/redis/modules
    cp ./target/release/librejson.so /usr/local/lib/redis/modules/
    echo "loadmodule /usr/local/lib/redis/modules/librejson.so" >> /etc/redis/redis.conf
}

main "$@"
