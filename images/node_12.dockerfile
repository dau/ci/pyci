FROM node:12
ENV HTTP_PROXY="http://proxy.esrf.fr:3128/"
ENV HTTPS_PROXY="http://proxy.esrf.fr:3128/"
ENV NO_PROXY="esrf.fr,localhost"

RUN echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list
RUN apt-get update -y
RUN apt-get install -y python3-pip build-essential jq
