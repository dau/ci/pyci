#!/bin/bash

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function main () {
    export version=$1

    cd ${SCRIPT_ROOT}
    rm -rf RediSearch

    git -c advice.detachedHead=false clone --recursive https://github.com/RediSearch/RediSearch.git --branch v${version}
    cd RediSearch

    sed -i '/extract_debug_symbols/d' CMakeLists.txt
    sed -i '/extract_debug_symbols/d' coord/CMakeLists.txt

    export CMAKE_CC_FLAGS='-UNDEBUG -fPIC -pthread -fno-strict-aliasing -O3 -Wno-error'
    export CMAKE_CC_C_FLAGS='-fcommon -Werror=incompatible-pointer-types -Werror=implicit-function-declaration'
    export CMAKE_CC_CXX_FLAGS=''
    export CMAKE_LD_FLAGS='-pthread'
    export CMAKE_SO_LD_FLAGS='-shared -Wl,-Bsymbolic,-Bsymbolic-functions'
    export CMAKE_EXE_LD_FLAGS=''
    export CMAKE_LD_FLAGS_LIST='-pthread'
    export CMAKE_SO_LD_FLAGS_LIST='-shared -Wl,-Bsymbolic,-Bsymbolic-functions'
    export CMAKE_EXE_LD_FLAGS_LIST=''
    export CMAKE_LD_LIBS='c m dl pthread rt'

    export PREFIX=$(pwd)/buildprefix
    mkdir -p $PREFIX

    cmake -Bbuild -H. -G "Ninja" -DCMAKE_INSTALL_PREFIX=$PREFIX \
                                -DCMAKE_FIND_ROOT_PATH=$PREFIX \
                                -DCMAKE_BUILD_TYPE=RelWithDebInfo \
                                -DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE=ONLY \
                                -DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY=ONLY
    cmake --build build

    mkdir -p /usr/local/lib/redis/modules
    cp ./build/redisearch.so /usr/local/lib/redis/modules/
    echo "loadmodule /usr/local/lib/redis/modules/redisearch.so" >> /etc/redis/redis.conf
}

main "$@"
