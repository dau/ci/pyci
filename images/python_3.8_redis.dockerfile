FROM python:3.8

RUN apt-get update -y
RUN apt-get install -y build-essential jq
RUN mkdir /build

##############################
# Install redis-server
##############################

COPY ./build-redis-server.sh /build
RUN chmod 755 /build/build-redis-server.sh
RUN /build/build-redis-server.sh 7.2.3

##############################
# Install redisjson module
##############################

RUN apt-get install -y cargo clang
COPY ./build-redisjson.sh /build
RUN chmod 755 /build/build-redisjson.sh
RUN /build/build-redisjson.sh 2.4.5
RUN apt-get remove -y cargo clang
RUN apt-get autoremove -y

##############################
# Install redissearch module
##############################

RUN apt-get install -y cmake ninja-build
COPY ./build-redissearch.sh /build
RUN chmod 755 /build/build-redissearch.sh
RUN /build/build-redissearch.sh 2.6.5
RUN apt-get remove -y cmake ninja-build
RUN apt-get autoremove -y

##############################
# Check redis-server
##############################

RUN redis-server --version
RUN cat /etc/redis/redis.conf
RUN ls /usr/local/lib/redis/modules/

##############################
# Cleanup
##############################

RUN rm -rf /build

##############################
# For ESRF gitlab runners
##############################

ENV HTTP_PROXY="http://proxy.esrf.fr:3128/"
ENV HTTPS_PROXY="http://proxy.esrf.fr:3128/"
ENV NO_PROXY="esrf.fr,localhost"
