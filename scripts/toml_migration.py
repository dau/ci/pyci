"""
Script to migrate setup.cfg to pyproject_toml. To be run in the project folder.
"""

import configparser
import os
from setuptools.config import setupcfg


def list_to_str(entries):
    result = "[\n"
    for entry in entries:
        entry = entry.replace('"', "'")
        result += f'    "{entry}",\n'
    result += "]"
    return result


cfg = setupcfg.read_configuration("setup.cfg")
raw_cfg = configparser.RawConfigParser()
raw_cfg.read("setup.cfg")

with open("pyproject.toml", "r") as f:
    old_toml = f.read()

with open("pyproject_new.toml", "w") as f:
    f.write(old_toml)
    if old_toml.endswith("\n"):
        f.write("\n")
    else:
        f.write("\n\n")

    readme = None
    license = None
    for file in os.listdir():
        if "README" in file:
            readme = file

        if "LICENSE" in file:
            license = file

    if readme is None:
        raise FileNotFoundError("No README found")

    if license is None:
        raise FileNotFoundError("No LICENSE found")

    project_section = [
        "[project]",
        f'name = "{cfg["metadata"]["name"]}"',
        f'version = "{cfg["metadata"]["version"]}"',
    ]
    keywords = cfg["metadata"].get("keywords", None)
    if keywords:
        project_section.append(f"keywords = {keywords}")

    project_section.extend(
        [
            'authors = [{name = "ESRF", email = "dau-pydev@esrf.fr"}]',
            f'description = "{cfg["metadata"]["description"]}"',
            f'readme = {{file = "{readme}", content-type = "text/markdown"}}',
            f'license = {{file = "{license}"}}',
            f'classifiers = {list_to_str(cfg["metadata"]["classifiers"])}',
            'requires-python = ">=3.8"',
            f'dependencies = {list_to_str(cfg["options"]["install_requires"])}',
        ]
    )

    f.write("\n".join(project_section))
    f.write("\n\n")

    repo_url = cfg["metadata"]["project_urls"]["Source"]
    project_urls_section = [
        "[project.urls]",
        f'Homepage = "{repo_url}"',
        f'Documentation = "{cfg["metadata"]["project_urls"]["Documentation"]}"',
        f'Repository = "{repo_url}"',
        f'Issues = "{os.path.join(repo_url, "issues")}"',
        f'Changelog = "{os.path.join(repo_url, "-/blob/main/CHANGELOG.md")}"',
    ]

    f.write("\n".join(project_urls_section))
    f.write("\n\n")

    extras = cfg["options"].get("extras_require", None)
    if extras:
        optional_deps_section = ["[project.optional-dependencies]"]

        for raw_extra in extras:
            extra = raw_extra.replace("_", "-")
            raw_deps = raw_cfg["options.extras_require"][extra].strip().split("\n")
            deps = []
            for raw_dep in raw_deps:
                # Deal with dependency interpolation: "%(test)s" must be replaced by "<project_name>[test]"
                if raw_dep.startswith("%(") and raw_dep.endswith(")s"):
                    deps.append(f'{cfg["metadata"]["name"]}[{raw_dep[2:-2]}]')
                else:
                    deps.append(raw_dep)

            optional_deps_section.append(f"{extra} = {list_to_str(deps)}")

        f.write("\n".join(optional_deps_section))
        f.write("\n\n")

    package_dir = [f'"{k}" = "{v}"' for k, v in cfg["options"]["package_dir"].items()]
    tool_sections = [
        "[tool.setuptools]",
        f"package-dir = {{ {' '.join(package_dir)} }}",
        "",
        "[tool.setuptools.packages.find]",
        'where = ["src"]',
        "",
    ]

    if "package_data" in cfg["options"]:
        tool_sections.append("[tool.setuptools.package-data]")
        for name, data in cfg["options"]["package_data"].items():
            tool_sections.append(f'"{name or "*"}"= {data}')
        tool_sections.append("")

    if "coverage:run" in raw_cfg:
        omit = raw_cfg["coverage:run"]["omit"].strip().split("\n")
        if "setup.py" in omit:
            omit.remove("setup.py")
        tool_sections.append("[tool.coverage.run]")
        tool_sections.append(f"omit = {omit}")
        tool_sections.append("")

    f.write("\n".join(tool_sections))
    f.write("\n")

    entry_points = cfg["options"].get("entry_points", [])
    entry_points_section = []

    if "console_scripts" in entry_points:
        entry_points_section.append("[project.scripts]")
        console_scripts = entry_points.pop("console_scripts")
        for raw_script in console_scripts:
            name, script = raw_script.split("=")
            entry_points_section.append(f'{name} = "{script}"')
        entry_points_section.append("")

    if "gui_scripts" in entry_points:
        entry_points_section.append("[project.gui-scripts]")
        gui_scripts = entry_points.pop("gui_scripts")
        for raw_script in gui_scripts:
            name, script = raw_script.split("=")
            entry_points_section.append(f'{name} = "{script}"')
        entry_points_section.append("")

    for entry_point_name in entry_points:
        entry_points_section.append(f'[project.entry-points."{entry_point_name}"]')
        for entry_point in entry_points[entry_point_name]:
            name, script = entry_point.split("=")
            entry_points_section.append(f'"{name}" = "{script}"')
        entry_points_section.append("")

    if entry_points_section:
        f.write("\n".join(entry_points_section))

with open(".flake8", "w") as f:
    f.write(
        """[flake8]
extend-ignore = E203,E501,E701
max-line-length = 88
exclude = [".eggs"]
"""
    )

with open(".gitignore", "r+") as f:
    lines = f.read().split("\n")

hidden_files_section = False
for i, line in enumerate(lines):
    if line.strip() == ".*":
        hidden_files_section = True
        continue

    if hidden_files_section:
        if line.startswith("!"):
            continue
        else:
            lines.insert(i, "!.flake8")
            break

if hidden_files_section:
    with open(".gitignore", "w") as f:
        f.write("\n".join(lines))

print(
    f"""New pyproject.toml was written in pyproject_new.toml.

    Remaining tasks:
     - Remove __version__ from __init__.py and use "import importlib.metadata; importlib.metadata.version("{cfg["metadata"]["name"]}")" where the version number is needed.
     - Check that `pyproject_new.toml` looks right, especially the list of dependencies.
     - Rename `pyproject_new.toml` to `pyproject.toml` and remove setup files.
     - Create a new Python env and try to run `pip install -e .[doc]`, `pytest`, `flake8`, `black` and `sphinx-build doc build/html`
     """
)
