#!/bin/bash

set -e

function show_help {
  echo "
        Build and Deploy docker images to Gitlab

        Usage: ./update_images.sh <username> [<match-pattern>] [-h]
        
        username        Gitlab user name.
        <match-pattern> Match pattern for the images in ./images (without extension).
        -h              Help
       "
}

function main {

    POSITIONAL_ARGS=()

    while [[ $# -gt 0 ]]; do
    case $1 in
        -h|--help)
        show_help
        return
        ;;
        -*|--*)
        echo "Unknown option $1"
        exit 1
        ;;
        *)
        POSITIONAL_ARGS+=("$1") # save positional arg
        shift # past argument
        ;;
    esac
    done

    docker login gitlab-registry.esrf.fr -u ${POSITIONAL_ARGS[0]}

    if [ "${#POSITIONAL_ARGS[@]}" -ge 2 ]; then
        filter="${POSITIONAL_ARGS[1]}.dockerfile"
    else
        filter="*.dockerfile"
    fi

    local tag="latest"
    for filename in ./images/$filter;do
        image=$(basename -- "$filename")
        image="${image%.*}"
        echo ""
        echo "------$image------"
        docker build --no-cache --tag=gitlab-registry.esrf.fr/dau/ci/pyci/$image:$tag --file=$filename ./images
        docker push gitlab-registry.esrf.fr/dau/ci/pyci/$image:$tag
        remove_docker_image gitlab-registry.esrf.fr/dau/ci/pyci/$image:$tag
    done
}

function remove_docker_image() {
    IMAGE_NAME_OR_ID="$1"

    CONTAINER_IDS=$(docker ps -a --filter ancestor="$IMAGE_NAME_OR_ID" -q)

    if [ -n "$CONTAINER_IDS" ]; then
        echo "Stopping and removing containers associated with the image..."
        docker stop $CONTAINER_IDS >/dev/null 2>&1
        docker rm $CONTAINER_IDS >/dev/null 2>&1
        echo "Containers removed."
    else
        echo "No containers associated with the image found."
    fi

    echo "Removing Docker image '$IMAGE_NAME_OR_ID'..."
    docker rmi "$IMAGE_NAME_OR_ID" >/dev/null 2>&1
    echo "Docker image removed."
}

main $@
