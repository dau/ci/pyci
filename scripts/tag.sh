#!/bin/bash

set -e

function show_help {
  echo "
        Tag project (current working directory)

        Usage: ./tag.sh tag [-h]

        tag         For example 1.0.0 will create the tag v1.0.0
        -h          Help
       "
}


function gittag {
    if git show-ref --quiet refs/heads/main;then
        git checkout main
    else
        git checkout master
    fi

    if [[ $? != 0 ]]; then
        return
    fi
    git pull
    if [[ $? != 0 ]]; then
        return
    fi
    git tag v$1
    if [[ $? != 0 ]]; then
        return
    fi
    git push --tags
}


function main {
    local tag

    POSITIONAL_ARGS=()

    while [[ $# -gt 0 ]]; do
    case $1 in
        -h|--help)
        show_help
        return
        shift # past argument
        shift # past value
        ;;
        -*|--*)
        echo "Unknown option $1"
        exit 1
        ;;
        *)
        POSITIONAL_ARGS+=("$1") # save positional arg
        shift # past argument
        ;;
    esac
    done

    tag=${POSITIONAL_ARGS[0]}

    if [ -z $tag ];then
        show_help
        return
    fi

    gittag $tag
}


main $@
