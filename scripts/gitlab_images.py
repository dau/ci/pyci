"""Script to generate Gitlab jobs to build all docker images."""

BUILD_DOCKER_BASE = """stages:
  - kaniko/py
  - kaniko/py_glx
  - kaniko/py_doc
  - kaniko/py_redis
  - kaniko/py_node
  - kaniko/other

.build_dockerfile:
  when: manual
  tags:
    - linux
"""

BUILD_DOCKER = """
build_{dockerfile}:
  stage: kaniko/{stage}
  extends: .build_dockerfile
  image: $CI_REGISTRY_IMAGE/kaniko:latest
  script:
    - /kaniko/executor
      --context "$CI_PROJECT_DIR/images"
      --dockerfile "$CI_PROJECT_DIR/images/{dockerfile}.dockerfile"
      --destination "$CI_REGISTRY_IMAGE/{dockerfile}:latest"
      --build-arg http_proxy="http://proxy.esrf.fr:3128/"
      --build-arg https_proxy="http://proxy.esrf.fr:3128/"
      --build-arg no_proxy="esrf.fr,localhost"
"""

BUILD_DOCKER_ = """
build_{dockerfile}:
  extends: .build_dockerfile
  script: echo ok
"""


def dockerfile_to_stage(dockerfile):
    if "python" not in dockerfile:
        return "other"
    if "redis" in dockerfile:
        return "py_redis"
    if "doc" in dockerfile:
        return "py_doc"
    if "glx" in dockerfile:
        return "py_glx"
    if "node" in dockerfile:
        return "py_node"
    return "py"


if __name__ == "__main__":
    import os
    from glob import glob

    thisdir = os.path.dirname(__file__)

    filename = os.path.join(thisdir, "..", ".gitlab-ci-images.yml")

    dockerfiles = glob(os.path.join(thisdir, "..", "images", "*.dockerfile"))

    with open(filename, mode="w") as f:
        f.writelines(BUILD_DOCKER_BASE)

        names = [os.path.splitext(os.path.basename(path))[0] for path in dockerfiles]
        for dockerfile in sorted(names):
            stage = dockerfile_to_stage(dockerfile)
            f.writelines(BUILD_DOCKER.format(dockerfile=dockerfile, stage=stage))
