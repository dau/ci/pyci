"""Script to generate Gitlab job templates."""

TEST_PY_LIN = """
.test-{pyversion}:
  extends: .test
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_{pyversion}:latest
"""

TEST_MINIFORGE_PY_LIN = """
.test-{pyversion}-miniforge3:
  extends: .test
  image: gitlab-registry.esrf.fr/dau/ci/pyci/miniforge3:latest
  before_script:
    - |
      for channel in $CONDA_CHANNELS; do
        mamba config --add channels $channel --prepend
      done
    - mamba info
    - mamba install python={pyversion} $CONDA_PACKAGES
    - !reference [.test, before_script]
  variables:
    CONDA_PACKAGES: ""
    CONDA_CHANNELS: ""
"""

TEST_PY_WIN = """
.test-{pyversion}-win:
  extends: .test-win
  variables:
    PYTHON_VER: "{pyversion}"
"""

TEST_SDIST_PY_LIN = """
.test_sdist-{pyversion}:
  extends: .test_sdist
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_{pyversion}:latest
"""

TEST_SDIST_MINIFORGE_PY_LIN = """
.test_sdist-{pyversion}-miniforge3:
  extends: .test_sdist
  image: gitlab-registry.esrf.fr/dau/ci/pyci/miniforge3:latest
  before_script:
    - |
      for channel in $CONDA_CHANNELS; do
        mamba config --add channels $channel --prepend
      done
    - mamba info
    - mamba install python={pyversion} $CONDA_PACKAGES
    - !reference [.test, before_script]
  variables:
    CONDA_PACKAGES: ""
    CONDA_CHANNELS: ""
"""


TEST_SDIST_PY_WIN = """
.test_sdist-{pyversion}-win:
  extends: .test_sdist-win
  variables:
    PYTHON_VER: "{pyversion}"
"""

TEST_PY_GLX_LIN = """
.test-{pyversion}_glx:
  extends: .test
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_{pyversion}_glx:latest
  variables:
    QT_QPA_PLATFORM: offscreen
"""

TEST_MINIFORGE_PY_GLX_LIN = """
.test-{pyversion}_glx-miniforge3:
  extends: .test
  image: gitlab-registry.esrf.fr/dau/ci/pyci/miniforge3_glx:latest
  before_script:
    - |
      for channel in $CONDA_CHANNELS; do
        mamba config --add channels $channel --prepend
      done
    - mamba info
    - mamba install python={pyversion} $CONDA_PACKAGES
    - !reference [.test, before_script]
  variables:
    CONDA_PACKAGES: ""
    CONDA_CHANNELS: ""
    QT_QPA_PLATFORM: offscreen
"""

TEST_PY_GLX_WIN = """
.test-{pyversion}_glx-win:
  extends: .test-win
  variables:
    QT_QPA_PLATFORM: offscreen
    PYTHON_VER: "{pyversion}"
"""

TEST_SDIST_PY_GLX_LIN = """
.test_sdist-{pyversion}_glx:
  extends: .test_sdist
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_{pyversion}_glx:latest
  variables:
    QT_QPA_PLATFORM: offscreen
"""

TEST_SDIST_MINIFORGE_PY_GLX_LIN = """
.test_sdist-{pyversion}_glx-miniforge3:
  extends: .test_sdist
  image: gitlab-registry.esrf.fr/dau/ci/pyci/miniforge3_glx:latest
  before_script:
    - |
      for channel in $CONDA_CHANNELS; do
        mamba config --add channels $channel --prepend
      done
    - mamba info
    - mamba install python={pyversion} $CONDA_PACKAGES
    - !reference [.test, before_script]
  variables:
    CONDA_PACKAGES: ""
    CONDA_CHANNELS: ""
    QT_QPA_PLATFORM: offscreen
"""

TEST_SDIST_PY_GLX_WIN = """
.test_sdist-{pyversion}_glx-win:
  extends: .test_sdist-win
  variables:
    QT_QPA_PLATFORM: offscreen
    PYTHON_VER: "{pyversion}"
"""

TEST_PY_REDIS_LIN = """
.test-{pyversion}-redis:
  extends: .test
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_{pyversion}_redis:latest
"""

TEST_SDIST_PY_REDIS_LIN = """
.test_sdist-{pyversion}-redis:
  extends: .test_sdist
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_{pyversion}_redis:latest
"""


def gitlab_pyjobs(pyversion: str, has_redis: bool):
    header = "### Optional jobs for python {pyversion} ###\n"
    header += TEST_PY_LIN
    header += TEST_PY_WIN
    header += TEST_SDIST_PY_LIN
    header += TEST_SDIST_PY_WIN
    header += TEST_PY_GLX_LIN
    header += TEST_PY_GLX_WIN
    header += TEST_SDIST_PY_GLX_LIN
    header += TEST_SDIST_PY_GLX_WIN
    if has_redis:
        header += TEST_PY_REDIS_LIN
        header += TEST_SDIST_PY_REDIS_LIN
    return header.format(pyversion=pyversion)


def gitlab_miniforge3_pyjobs(pyversion):
    header = "### Optional jobs for python {pyversion} under miniforge3###\n"
    header += TEST_MINIFORGE_PY_LIN
    header += TEST_SDIST_MINIFORGE_PY_LIN
    header += TEST_MINIFORGE_PY_GLX_LIN
    header += TEST_SDIST_MINIFORGE_PY_GLX_LIN
    return header.format(pyversion=pyversion)


if __name__ == "__main__":
    import os
    import re
    from glob import glob

    thisdir = os.path.dirname(__file__)

    dockerfiles = glob(os.path.join(thisdir, "..", "images", "*.dockerfile"))

    version_pattern = re.compile(r"python_(\d+\.\d+)")
    py_versions = sorted(
        set(version_pattern.findall(" ".join(dockerfiles))),
        key=lambda s: tuple(map(float, s.split("."))),
    )

    version_pattern = re.compile(r"python_(\d+\.\d+)_redis")
    py_versions_with_redis = sorted(
        set(version_pattern.findall(" ".join(dockerfiles))),
        key=lambda s: tuple(map(float, s.split("_")[0].split("."))),
    )

    filename = os.path.join(os.path.dirname(__file__), "..", ".gitlab-ci-pyjobs.yml")

    with open(filename, mode="w") as f:
        for pyversion in py_versions:
            has_redis = pyversion in py_versions_with_redis
            f.writelines(gitlab_pyjobs(pyversion, has_redis))
            f.writelines("\n")

        for pyversion in py_versions:
            f.writelines(gitlab_miniforge3_pyjobs(pyversion))
            f.writelines("\n")
