# CI template for python projects

This project reduces the Gitlab CI boiler plate for python projects.

Example usage:

```yaml
# Add black and flake8 jobs + provide job templates for testing, building and documentation
include:
  - project: dau/ci/pyci
    file: .gitlab-ci-template.yml

# Test source installation on Linux
test-3.11:
  extends: .test-3.11

# Test source installation on Windows
test-3.11-win:
  extends: .test-3.11-win

# Build source distribution
build_sdist:
  extends: .build_sdist

# Test package installation on Linux
test_sdist-3.11:
  extends: .test_sdist-3.11

# Test package installation on Windows
test_sdist-3.11-win:
  extends: .test_sdist-3.11-win

# Build the sphinx documentation
build_doc:
  extends: .build_doc

# Deploy the sphinx documentation on gitlab
pages:
  extends: .pages

# Store the source distribution
assets:
  extends: .assets
```

## Add a new Python version

1. Add the `Dockerfile` of the new images (classic, `glx` and `doc`) in the `images/` folder

1. Run the script `scripts/gitlab_generate.sh`

Follow python releases: https://devguide.python.org/versions/

## Build and deploy Docker images on Gitlab

The CI of this project has a job to build and deploy each image. These jobs can be trigger manually.

## Build and deploy Docker images locally

Build and deploy images to Gitlab from your local computer (requires docker)

```bash
sudo ./scripts/update_images.sh ${USER} [<match-pattern>]
```

### Build image

```bash
sudo docker build -t gitlab-registry.esrf.fr/dau/ci/pyci/python_3.11:latest -f images/python_3.11.dockerfile ./images
```

### Test image

```bash
sudo docker images
sudo docker run -i -t gitlab-registry.esrf.fr/dau/ci/pyci/python_3.11:latest /bin/bash
```

### Upload image to gitlab

```bash
sudo docker login gitlab-registry.esrf.fr -u ${USER}
sudo docker push gitlab-registry.esrf.fr/dau/ci/pyci/python_3.11:latest
```

### Run image

```bash
sudo docker login gitlab-registry.esrf.fr -u ${USER}
sudo docker run -it --ulimit core=-1 --init -v /users/${USER}:/home gitlab-registry.esrf.fr/dau/ci/pyci/python_3.11:latest /bin/bash
```

### Clean locally

```bash
sudo docker system prune -a -f
```

## Helper scripts for python projects

### Deploy on pypi

Release the gitlab CI artifacts with the
[deploy script](https://gitlab.esrf.fr/dau/ci/pyci/-/blob/main/scripts/deploy.sh)

```bash
./scripts/deploy.sh project_name [-n group/subgroup]
```

or

```bash
curl -sSL https://gitlab.esrf.fr/dau/ci/pyci/-/raw/main/scripts/deploy.sh | bash -s -- project_name [-n group/subgroup]
```

### Git tag releases

You can use the [tag script](https://gitlab.esrf.fr/dau/ci/pyci/-/blob/main/scripts/tag.sh)
to tag the tip of the default branch

```bash
./scripts/tag.sh 1.2.3
```

or

```bash
curl -sSL https://gitlab.esrf.fr/dau/ci/pyci/-/raw/main/scripts/tag.sh | bash -s -- 1.2.3
```

### Migrate setup.cfg to pyproject_toml

Run in the project folder this script in the project folder to be migrated

```bash
python scripts/toml_migration.py
```

or

```bash
curl -sSL https://gitlab.esrf.fr/dau/ci/pyci/-/raw/main/scripts/toml_migration.py | python
```
